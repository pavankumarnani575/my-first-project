class Iterable:
    '''
    class Iterable(object)
     |  The object must be an iterable like list.
     |  Iterable(object) -> list
     |  Creates a new list object
    '''
    def __init__(self, iterable):
        self.iterable = iterable

    def __str__(self):
        return str(self.iterable)

    def __repr__(self):
        return str(self.iterable)

    def selectionsort(self):
        '''
        Algorithm used for sorting = Selection Sort

        Parameters
        ----------
        Does not take any parameters but the works only on the Iterable object.

        Returns
        -------
        a sorted list

        '''
        array = self.iterable.copy()
        size = len(array)
        for step in range(size):
            min_idx = step
            for i in range(step + 1, size):
                if array[i] < array[min_idx]:
                    min_idx = i
            (array[step], array[min_idx]) = (array[min_idx], array[step])
        return array

    def bubblesort(self):
        '''
        Algorithm used for sorting = Bubble Sort

        Parameters
        ----------
        Does not take any parameters but the works only on the Iterable object.

        Returns
        -------
        a sorted list

        '''
        array = self.iterable.copy()
        for i in range(len(array)):
            for j in range(len(array) - 1 - i):
                if array[j] > array[j + 1]:
                    array[j], array[j + 1] = array[j + 1], array[j]

        return array

    def insertionsort(self):
        '''
        Algorithm used for sorting = Insertion Sort

        Parameters
        ----------
        Does not take any parameters but the works only on the Iterable object.

        Returns
        -------
        a sorted list

        '''
        array = self.iterable.copy()
        for i in range(1, len(array)):
            key = array[i]
            j = i - 1
            while j >= 0 and key < array[j]:
                array[j + 1] = array[j]
                j = j - 1
            array[j + 1] = key

        return array

    def mergesort(self):
        '''
        Algorithm used for sorting = Merge Sort

        Parameters
        ----------
        Does not take any parameters but the works only on the Iterable object.

        Returns
        -------
        a sorted list

        '''
        array = self.iterable.copy()
        def tempsort(array):
            if len(array) > 1:

                r = len(array) // 2
                L = array[:r]
                M = array[r:]

                tempsort(L)
                tempsort(M)

                i = j = k = 0

                while i < len(L) and j < len(M):
                    if L[i] < M[j]:
                        array[k] = L[i]
                        i += 1
                    else:
                        array[k] = M[j]
                        j += 1
                    k += 1

                while i < len(L):
                    array[k] = L[i]
                    i += 1
                    k += 1

                while j < len(M):
                    array[k] = M[j]
                    j += 1
                    k += 1
            return array

        return tempsort(array)

    def quicksort(self):
        '''
        Algorithm used for sorting = Quick Sort

        Parameters
        ----------
        Does not take any parameters but the works only on the Iterable object.

        Returns
        -------
        a sorted list

        '''
        array = self.iterable.copy()
        low = 0
        high = len(array) - 1

        def partition(array, low, high):
            pivot = array[high]
            i = low - 1

            for j in range(low, high):
                if array[j] <= pivot:
                    i = i + 1
                    (array[i], array[j]) = (array[j], array[i])

            (array[i + 1], array[high]) = (array[high], array[i + 1])

            return i + 1

        def tempsort(array, low, high):
            if low < high:
                pi = partition(array, low, high)

                tempsort(array, low, pi - 1)

                tempsort(array, pi + 1, high)

            return array

        return tempsort(array, low, high)

    def heapsort(self):
        '''
        Algorithm used for sorting = Heap Sort

        Parameters
        ----------
        Does not take any parameters but the works only on the Iterable object.

        Returns
        -------
        a sorted list

        '''
        array = self.iterable.copy()

        def heapify(arr, n, i):
            largest = i
            l = 2 * i + 1
            r = 2 * i + 2

            if l < n and arr[i] < arr[l]:
                largest = l

            if r < n and arr[largest] < arr[r]:
                largest = r

            if largest != i:
                arr[i], arr[largest] = arr[largest], arr[i]
                heapify(arr, n, largest)

        def tempsort(arr):
            n = len(arr)

            for i in range(n // 2, -1, -1):
                heapify(arr, n, i)

            for i in range(n - 1, 0, -1):
                arr[i], arr[0] = arr[0], arr[i]

                heapify(arr, i, 0)
            return array

        return tempsort(array)

    def countsort(self):
        '''
        Algorithm used for sorting = Count Sort

        Parameters
        ----------
        Does not take any parameters but the works only on the Iterable object.

        Returns
        -------
        a sorted list

        '''
        array = self.iterable.copy()

        def tempsort(array):
            size = len(array)
            output = [0] * size

            count = [0] * 10

            for i in range(0, size):
                count[array[i]] += 1

            for i in range(1, 10):
                count[i] += count[i - 1]

            i = size - 1
            while i >= 0:
                output[count[array[i]] - 1] = array[i]
                count[array[i]] -= 1
                i -= 1

            for i in range(0, size):
                array[i] = output[i]
            return array

        return tempsort(array)

    def radixsort(self):
        '''
        Algorithm used for sorting = Radix Sort

        Parameters
        ----------
        Does not take any parameters but the works only on the Iterable object.

        Returns
        -------
        a sorted list

        '''
        array = self.iterable.copy()

        def countingSort(array, place):
            size = len(array)
            output = [0] * size
            count = [0] * 10

            for i in range(0, size):
                index = array[i] // place
                count[index % 10] += 1

            for i in range(1, 10):
                count[i] += count[i - 1]

            i = size - 1
            while i >= 0:
                index = array[i] // place
                output[count[index % 10] - 1] = array[i]
                count[index % 10] -= 1
                i -= 1

            for i in range(0, size):
                array[i] = output[i]

        def tempsort(array):
            max_element = max(array)
            place = 1
            while max_element // place > 0:
                countingSort(array, place)
                place *= 10

            return array

        return tempsort(array)

    def linearsearch(self, x):
        '''
        Algorithm used for searching = Linear Search

        Parameters
        ----------
        Takes only one parameter which is the object to be searched in an iterable.

        Returns
        -------
        index of the object in that iterable if found
        Not Found if not the object is not found

        '''
        array = self.iterable.copy()
        n = len(array)
        for i in range(0, n):
            if (array[i] == x):
                return i
        return "Not Found"

    def binarysearch(self, x):
        '''
        Algorithm used for searching = Binary Search

        Parameters
        ----------
        Takes only one parameter which is the object to be searched in an iterable.

        Returns
        -------
        index of the object in that iterable if found
        Not Found if not the object is not found

        '''
        array = self.iterable.copy()
        array = self.bubblesort()

        low = 0
        high = len(array) - 1

        while low <= high:

            mid = low + (high - low) // 2

            if array[mid] == x:
                return mid

            elif array[mid] < x:
                low = mid + 1

            else:
                high = mid - 1

        return "Not Found"

    def jumpsearch(self, search):
        '''
        Algorithm used for searching = Jump Search

        Parameters
        ----------
        Takes only one parameter which is the object to be searched in an iterable.

        Returns
        -------
        index of the object in that iterable if found
        Not Found if not the object is not found

        '''
        array = self.iterable.copy()
        low = 0
        interval = int((len(array)) ** 0.5)
        for i in range(0, len(array), interval):
            if array[i] < search:
                low = i
            elif array[i] == search:
                return i
            else:
                break
        c = low
        for j in array[low:]:
            if j == search:
                return c
            c += 1
        return "Not found"

    def exponentialsearch(self, val):
        '''
        Algorithm used for searching = Exponential Search

        Parameters
        ----------
        Takes only one parameter which is the object to be searched in an iterable.

        Returns
        -------
        index of the object in that iterable if found
        Not Found if not the object is not found

        '''
        lys = self.iterable.copy()
        lys = self.bubblesort()

        def BinarySearch(lys, val):
            first = 0
            last = len(lys) - 1
            index = -1
            while (first <= last) and (index == -1):
                mid = (first + last) // 2
                if lys[mid] == val:
                    index = mid
                else:
                    if val < lys[mid]:
                        last = mid - 1
                    else:
                        first = mid + 1
            return index

        def tempsearch(lys, val):
            if lys[0] == val:
                return 0
            index = 1
            while index < len(lys) and lys[index] <= val:
                index = index * 2
            return BinarySearch(lys[:min(index, len(lys))], val)

        index = tempsearch(lys, val)
        if index != -1:
            return index
        else:
            return "Not Found"

    def sublistsearch(self, s):
        '''
        Algorithm used for searching = Sublist Search

        Parameters
        ----------
        Takes only one parameter which is sub list to be searched in the main list.

        Returns
        -------
        List Found if the sublist present in the main list
        List Not Found if the sublist not present in the main list

        '''
        l = self.iterable.copy()
        sub_set = False
        if s == []:
            sub_set = True
        elif s == l:
            sub_set = True
        elif len(s) > len(l):
            sub_set = "LIST NOT FOUND"

        else:
            for i in range(len(l)):
                if l[i] == s[0]:
                    n = 1
                    while (n < len(s)) and (l[i + n] == s[n]):
                        n += 1

                    if n == len(s):
                        sub_set = "LIST FOUND"

        return sub_set
